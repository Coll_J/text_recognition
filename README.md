# Text Recognition
Text detection using OpenCV DNN, EAST Detection, Tesseract

## Created by
Zakiya Azizah C 05111840000080
and friends (KCV - 1) xixi

## Usage

Refer to the following to use the Python script:

```
python text_detection.py --input <image_path>
```
*zero input to use webcam*

## Setup

1. opencv, installation [here](https://docs.opencv.org/3.4/d2/de6/tutorial_py_setup_in_ubuntu.html)
2. pygame, installation `sudo pip install pygame`
3. tesseract, installation `sudo apt-get install tesseract-ocr`
